import numpy as np
from PIL import Image
import mazedownloader
import funcs

mazedownloader.downloadimg()

img = Image.open("image.png")
# img = Image.open("maze10.png")
img = img.convert("L")
imgX = img.size[0]
imgY = img.size[1]

arr = np.empty(shape=(imgX,imgY/2), dtype = int)
data = list(img.getdata())
co = 0
idx = 0

#putting stuff into the array for the 1st time
for i in range(0,arr.shape[0]):
    for j in range(0,arr.shape[1]):
        if(data[idx]==255):
            arr[i][j] = 0
        elif(data[idx]==0):
            arr[i][j] = 1
            co = co + 1
        idx = idx + 2

arr = np.rot90(arr,k=1)
arr2 = np.empty(dtype = int, shape=(imgX/2,imgY/2))
idx = 0
for i in range(0,arr2.shape[0]):
    for j in range(0,arr2.shape[1]):
        arr2[i][j] = arr[i][idx]
        idx = idx + 2
    idx = 0

arr2 = np.rot90(arr2,k=3)
# np.savetxt("mtrxfinal.txt",arr2,fmt='%i')

#placing horizontal walls
wallsh = open("wallsh.txt","w")
hstr = ""
for i in range(0,arr2.shape[0]):
    h_list = funcs.gethcoordlist(funcs.getlist(arr2[i]),i)
    if(h_list!=[]):
        st = ""
        for x,y in h_list:
            st = "CreateObject(577,Maze.World,Vector(Maze.Origin.x+"+str(x)+",Maze.Origin.y-"+str(y)+",Maze.Origin.z),255);"
            wallsh.write(st+"\n")
            st = ""
arr2 = np.transpose(arr2)

#placing vertical walls
wallsv = open("wallsv.txt","w")
vstr = ""
for i in range(0,arr2.shape[0]):
    v_list = funcs.getvcoordlist(funcs.getlist(arr2[i]),i)
    if(v_list!=[]):
        st = ""
        for x,y in v_list:
            st = "CreateObject(577,Maze.World,Vector(Maze.Origin.x+"+str(x)+",Maze.Origin.y-"+str(y)+",Maze.Origin.z),255).RotateToEuler(Vector(0,0,1.570796),1);"
            wallsv.write(st+"\n")
            st = ""
