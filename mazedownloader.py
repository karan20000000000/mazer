# coding: utf-8
from splinter import Browser
import urllib2

def downloadimg(width=10,height=10, Eval = 50, Rval = 100):
    # bro = Browser('phantomjs')
    with Browser('phantomjs') as bro:
        bro.visit("http://www.mazegenerator.net")

        #maze config
        bro.find_by_id('S1WidthTextBox').first.fill(str(width))
        bro.find_by_id('S1HeightTextBox').first.fill(str(height))
        bro.find_by_id('AlgorithmParameter1TextBox').first.fill(str(Eval))
        bro.find_by_id('AlgorithmParameter2TextBox').first.fill(str(Rval))


        bro.click_link_by_id("GenerateButton")
        src = bro.find_by_id("MazeDisplay")['src']
        src = src.replace("Image","Png")
        cook = bro.cookies.all()
        s = "; ".join('%s=%s' %(k,v) for k,v in cook.items())

        opener = urllib2.build_opener()
        opener.addheaders.append(('Cookie',s))
        op = opener.open(src)
        imgtxt = op.read()
        imgfile = open("image.png","wb")
        imgfile.write(imgtxt)
        imgfile.close()

if __name__ == '__main__':
    downloadimg()
