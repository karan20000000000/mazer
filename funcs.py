import numpy as np

def getlist(arr):
    ls = []
    ls.append(arr[0])

    current = arr[0]
    idx = 0
    ctr = 0
    while(True):
        if(idx==arr.shape[0]):
            ls.append(ctr)
            ctr = 0
            break
        elif(arr[idx] != current):
            ls.append(ctr)
            ctr = 0
            current = arr[idx]

        elif(arr[idx]==current):
            ctr = ctr + 1
            idx = idx + 1

    return ls

def gethcoordlist(arr,rowid):
    current = arr[0]
    ls = []
    x = -2.21666
    y = rowid*0.4926

    for i in range(1,len(arr)):
        if(arr[i]==1):
            x = x + 0.4926
            current = 0 if current==1 else 1
            continue
        #we've to place walls
        if(current==1):
            px = arr[i]
            num = px/9

            #place num walls
            for j in range(0,num):
                x = x + 4.43332
                ls.append((x,y))

            #check the last wall and adjust its x coordinate
            if(px%9 != 0):
                xc = (px%9)*0.4926
                x  = x + xc
                ls.append((x,y))
            current = 0

        #we've to leave space, 1px = 0.4926 game units
        elif(current==0):
            for j in range(0,arr[i]):
                x = x + 0.4926
            current = 1

    return ls

def getvcoordlist(arr,rowid):
    current = arr[0]
    ls = []
    y = -2.21666
    x = rowid*0.4926

    for i in range(1,len(arr)):
        #we dont want to place a huge ass wall for 1 pixel
        if(arr[i]==1):
            y = y + 0.4926
            current = 0 if current==1 else 1
            continue
        #we've to place walls
        if(current==1):
            px = arr[i]
            num = px/9

            #place num walls
            for j in range(0,num):
                y = y + 4.43332
                ls.append((x,y))

            #check the last wall and adjust its x coordinate
            if(px%9 != 0):
                yc = (px%9)*0.4926
                y  = y + yc
                ls.append((x,y))
            current = 0

        #we've to leave space, 1px = 0.4926 game units
        elif(current==0):
            for j in range(0,arr[i]):
                y = y + 0.4926
            current = 1

    return ls
