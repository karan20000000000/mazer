import numpy as np

from PIL import Image
img = Image.open("maze4.png")
img = img.convert("L")
imgX = img.size[0]
imgY = img.size[1]
file = open("mtrx.txt","w")
file2 = open("data.txt","w")
# def getimg():
#     img = Image.open("maze4.png")
#     img = img.convert("L")
#     pass
#
# getimg()
Matrix = [[0 for x in range(img.size[0]/2)] for y in range(img.size[1])]


data = list(img.getdata())

co = 0
idx = 0
for i in range(0,img.size[0]):
    for j in range(0,img.size[1]/2):
        if(data[idx]==255):
            Matrix[i][j] = 0
        elif(data[idx]==0):
            Matrix[i][j] = 1
            co = co+1
        idx = idx + 2

arr = np.array(Matrix,dtype=int)
arr = np.rot90(arr)
arr2 = np.empty(dtype=int,shape=(imgX/2,imgY/2))

for i in range(0,arr2.shape[0]):
    for j in range(0,arr2.shape[1]):
        arr2[i][j] = arr[i][j+2]

arr2 = np.rot90(arr2,k=3)
np.savetxt("mtrx.txt",arr2,fmt='%i')
