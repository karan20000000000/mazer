import numpy as np
from PIL import Image

img = Image.open("urltest.png")
img = img.convert("L")
imgX = img.size[0]
imgY = img.size[1]

arr = np.empty(shape=(imgX,imgY/2), dtype = int)
data = list(img.getdata())
co = 0
idx = 0

#putting stuff into the array for the 1st time
for i in range(0,arr.shape[0]):
    for j in range(0,arr.shape[1]):
        if(data[idx]==255):
            arr[i][j] = 0
        elif(data[idx]==0):
            arr[i][j] = 1
            co = co + 1
        idx = idx + 2

np.savetxt("mtrx.txt",arr,fmt='%i')

arr = np.rot90(arr,k=1)

# np.savetxt("mtrx.txt",arr,fmt='%i')
arr2 = np.empty(dtype = int, shape=(imgX/2,imgY/2))

idx = 0
for i in range(0,arr2.shape[0]):
    for j in range(0,arr2.shape[1]):
        arr2[i][j] = arr[i][idx]
        idx = idx + 2
    idx = 0

np.savetxt("mtrxrot90.txt",arr2,fmt='%i')

arr2 = np.rot90(arr2,k=3)
np.savetxt("mtrxfinal.txt",arr2,fmt='%i')
